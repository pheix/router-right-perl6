# Advanced URL router in Raku

Advanced, framework-agnostic URL routing engine for web applications in [Raku](https://raku.org) programming language:

* [Synopsys](#synopsys)
* [Methods](#methods)
* [Examples](#examples)
* [Wiki pages](#wiki-pages)
* [Author](#author)
* [Credits](#credits)

## Synopsys

```perl
use v6.d;
use Router::Right;

my $route = Router::Right.new;

$route.add(
    :name( 'dynaroute' ),
    :path( 'GET /dynamic/{id:<[\d]>+}/{tag}/{item}' ),
    :payload( 'Dynamic#Route' ),
);

if !(
    my %m = $route.match(
        %*ENV<REQUEST_URI> || ~(),
    )
) {
    say $route.error;
}
else {
    say %m.gist;
}
```

## Methods

* new() — returns a new Router::Right instance;
* [add()](https://gitlab.com/pheix/raku-router-right/wikis/add-method) — defines a new route;
* [match()](https://gitlab.com/pheix/raku-router-right/wikis/match-method) — attempts to find a route that matches the supplied URL;
* [error()](https://gitlab.com/pheix/raku-router-right/wikis/error-method) — returns the error code of the last failed match;
* [allowed_methods()](https://gitlab.com/pheix/raku-router-right/wikis/allowed_methods-method) — returns the array of allowed methods for a given route;
* [url()](https://gitlab.com/pheix/raku-router-right/wikis/url-method) — constructs a URL from the route;
* [as_string()](https://gitlab.com/pheix/raku-router-right/wikis/as_string-method) — returns a report of the defined routes, in order of definition;
* [with()](https://gitlab.com/pheix/raku-router-right/wikis/with-method) — helper method to share information across multiple routes;
* [resource()](https://gitlab.com/pheix/raku-router-right/wikis/resource-method) — adds routes to create, read, update, and delete a given resource;
* [route()](https://gitlab.com/pheix/raku-router-right/wikis/route-method) — returns route details hash.

## Examples

* [Hello, world!](https://gitlab.com/pheix/raku-router-right/wikis/hello-world-example) — simple router;
* [Trick with resources](https://gitlab.com/pheix/raku-router-right/wikis/trick-with-resources-example) — populate resources with nested [with()](https://gitlab.com/pheix/raku-router-right/wikis/with-method) call;

## Wiki pages

Full documentation is available at `Router::Right` [wiki pages](https://gitlab.com/pheix/raku-router-right/wikis/home).

## License

`Router::Right` module is free and open source software, so you can redistribute it and/or modify it under the terms of the [Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Author

Please contact me via [Matrix](https://matrix.to/#/@k.narkhov:matrix.org) or [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

## Credits

This module was inspired by origin Perl5 `Router::Right` developed by [@mla](mailto:maurice.aubrey@gmail.com). Check it out at [Github](https://github.com/mla/router-right).
