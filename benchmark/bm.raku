#!/usr/bin/env perl6
use v6.d;

use lib './lib','../lib';

#use Cro;
#use Cro::HTTP::Request;
#use Cro::HTTP::Router;
use Router::Right;
use Router::Boost;
use Text::Table::Simple;
use Benchmark;

my Bool $debug = False;
my @data;

sub word {
    my $word = shift @data;
    push @data, $word;
    return $word;
}

sub MAIN(Int $routes = 100) {
    my @paths;
    my Str $fn = './dictionary.txt';
    if $fn.IO.e {
        @data = $fn.IO.slurp.lines.map({ $_ if $_ });
        if @data {
            my $r  = Router::Right.new;
            my $rb = Router::Boost.new;
            while (@paths.elems < $routes) {
                my Int $num_words = 3.rand.floor + 1;
                my Str $path = q{/} ~ (1..$num_words).map({ word() }).join(q{/});
                my Int $idx = @paths.elems + 1;
                if $debug {
                    ( 'Added route: ' ~ $path).say;
                }
                $r.add( :name( 'name' ~ $idx ), :path( $path ), :payload( %(controller => 'Foo_' ~ $idx) ) );
                $rb.add( $path, 'name' ~ $idx );
                @paths.push($path);

                my Str $path_with_id = $path ~ '/{id}';
                if $debug {
                    ( 'Added route: ' ~ $path_with_id).say;
                }
                $r.add( :name( 'name_id' ~ $idx ), :path($path_with_id), :payload( %(controller => 'FooId_' ~ $idx) ) );
                $rb.add( $path_with_id, 'name_id' ~ $idx );
                $path_with_id ~~ s/\{id\}/$idx/;
                @paths.push($path_with_id);
                $idx++;
            }
            if @paths {
                my UInt $i = 0;
                my UInt $j = 0;
                my UInt $k = 0;

                # my $app = route {
                #    for @paths -> $path {
                #        my ($rpt1, $rpt2, $rpt3) = $path.split(q{/}, :skip-empty);

                #         if !$rpt3 {
                #             get ->$rpt1, $rpt2 {
                #                 response.status = 200;
                #                 response.append-header('Content-type', 'text/html');
                #                 response.set-body(("This is route: /$rpt1/$rpt2").encode('ascii'));
                #             };
                #         }
                #         else {
                #             get ->$rpt1, $rpt2, $rpt3 {
                #                 response.status = 200;
                #                 response.append-header('Content-type', 'text/html');
                #                 response.set-body(("This is route: /$rpt1/$rpt2/$rpt3").encode('ascii'));
                #             };
                #         }
                #     }
                # }

                # my $source = Supplier.new;
                # my $responses = $app.transformer($source.Supply).Channel;

                my %results = timethese($routes, {
                    'Router::Right' => sub {
                        my %h = $r."match"(@paths[$i++], Str);
                        # %h.gist.say;
                        # q{.}.print;
                    },
                    'Router::Boost' => sub {
                        my %h = $rb."match"(@paths[$j++]);
                        # %h.gist.say;
                        # q{+}.print;
                    },
                    # 'Cro::HTTP::Router' => sub {
                    #     my $route = @paths[$k++].encode("ascii", :replacement(q{})).decode("ascii");
                    #     $source.emit(Cro::HTTP::Request.new(:method<GET>, :target($route)));
                    #     given $responses.receive -> $r {
                    #         if !($r ~~ Cro::HTTP::Response && $r.status) {
                    #             "path $route is skipped by Cro::HTTP::Router".say;
                    #         }
                    #     }
                    # }
                });
                "\n".print;
                # %results.gist.say;
                my @rows;
                my @headers = ['func','start','end','diff','avg'];
                for %results.kv -> $k, $v {
                    my @_r.append($k).append(($v).map({$_})).push();
                    @rows.push(@_r);
                    # @_r.say;
                }
                my @table = lol2table(@headers, @rows );
                $_.say for @table;
            }
        }
    }
}
