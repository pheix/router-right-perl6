unit module Benchmark;
use MONKEY-SEE-NO-EVAL;

my sub time_it (Int $count where { $_ > 0 }, Code $code) {
    my $timestamp = now.Rat;
    for 1..$count { $code.(); }
    my $delta = now.Rat - $timestamp;
    my $average = $delta / $count;
    return ($timestamp, ($delta + $timestamp), $delta, $average);
}

multi sub timethis (Int $count, Str $code) is export {
    my $routine = { EVAL $code };
    return time_it($count, $routine);
}

multi sub timethis (Int $count, Code $code) is export {
    return time_it($count, $code);
}

sub timethese (Int $count, %h) is export {
    my %results;
    for %h.kv -> $k, $sub {
        %results{$k} = timethis($count, $sub);
    }
    return %results;
}
