use v6;
use Test;

use Router::Right;

plan 9;

my %h;
my @a;
my $r = Router::Right.new;
my Bool $raise_except;

subtest {
    plan 5;
    @a = $r.split_route_path('GET|POST /route');
    ok(
        ( @a[0] eq 'GET|POST' && @a[1] eq '/route' ),
        'split_route_path test no.1',
    );
    @a = $r.split_route_path('GET /route/subroute/');
    ok(
        ( @a[0] eq 'GET' && @a[1] eq '/route/subroute/' ),
        'split_route_path test no.2',
    );
    @a = $r.split_route_path('GET|PUT|DELETE /route/sub/sub/');
    ok(
        ( @a[0] eq 'GET|PUT|DELETE' && @a[1] eq '/route/sub/sub/' ),
        'split_route_path test no.3',
    );
    @a = $r.split_route_path('  POST /a/b/c/d');
    ok(
        ( @a[0] eq 'POST' && @a[1] eq '/a/b/c/d' ),
        'split_route_path test no.4',
    );
    @a = $r.split_route_path( "\t\t" ~ ' POST ' ~ "\t" ~ '/a/b/');
    ok(
        ( @a[0] eq 'POST' && @a[1] eq '/a/b/' ),
        'split_route_path test no.5',
    );
}, 'check split_route_path method';

subtest {
    plan 3;
    my @a_etalon = ('DELETE', 'GET', 'POST', 'PUT');
    @a = $r.methods('GET|POST,pUt', 'GET,post|DelEte');
    is-deeply(@a, @a_etalon, 'method methods test no.1');
    @a_etalon = ('DELETE', 'GET', 'POST');
    @a = $r.methods('', 'GET,post|DelEte');
    is-deeply(@a, @a_etalon, 'method methods test no.2');
    @a_etalon = ('GET', 'POST', 'PUT');
    @a = $r.methods('GET,post|PuT', '');
    is-deeply(@a, @a_etalon, 'method methods test no.3');
}, 'check methods method';

subtest {
    plan 2;
    my @route = $r.build_route( '/entries/{year:\d+}/{month:\d+}' );
    is(
        @route[1],
        '\/entries\/[$<year>=(\d+)]\/[$<month>=(\d+)]',
        'build_route test no.1',
    );
    @route = $r.build_route( '/dl/{file}{.format}' );
    is(
        @route[1],
        '\/dl\/[$<file>=(<-[/]>+:?)][\.$<format>=(<-[.\s/]>+:?)]?',
        'build_route test no.2',
    );
}, 'check build_route method';

subtest {
    plan 3;
    my %h_etalon = controller => 'Controller', action => 'Action';
    my %h = $r.parse_payload('Controller#Action');
    is-deeply(%h, %h_etalon, 'method parse_payload test no.1');
    %h_etalon = controller => 'Controller';
    %h = $r.parse_payload('Controller');
    is-deeply(%h, %h_etalon, 'method parse_payload test no.2');
    %h_etalon = action => 'Action';
    %h = $r.parse_payload('#Action');
    is-deeply(%h, %h_etalon, 'method parse_payload test no.3');
}, 'check parse_payload method';

subtest {
    plan 4;
    ok(
        (
            $r.add(
                :name('route2'),
                :path('GET|POST /entries/{year:\d+}/{month:\d+}'),
                :payload(
                    %(
                        controller => 'Entries',
                        action => 'show',
                    )
                ),
            ) ~~ Router::Right
        ),
        'add method w/o options',
    );
    ok(
        (
            $r.add(
                :name('entries'),
                :path('/dl/{file}{.format}'),
                :payload(
                    %(
                        controller => 'Dl',
                        action => 'readfile',
                    )
                ),
                :options('GET | POST'),
            ) ~~ Router::Right
        ),
        'add method with options',
    );
    ok(
        (
            $r.add(
                :name('feedback'),
                :path('/feedback/{name:<[a..zA..Z]>+}/{nick}/{email}'),
                :payload(
                    %(
                        controller => 'Feedback',
                        action => 'getuserdata',
                    )
                ),
                :options('GET | POST'),
            ) ~~ Router::Right
        ),
        'add method with wildcards',
    );
    ok(
        (
            $r.add(
                :name('weblog'),
                :path('/weblog/{page:<[a..zA..Z0..9]>+}'),
                :payload('Controller#Weblog'),
                :options('GET | POST'),
            ) ~~ Router::Right
        ),
        'add method with payload wtring and wildcards',
    );
}, 'check add method';

subtest {
    plan 3;
    my Str $url = $r.url('route2', %( year => '1916', month => '08' ));
    is($url, '/entries/1916/08', 'url method test no.1');
    $url = $r.url(
        '/entry/{page:<[a..zA..Z0..9]>+}/',
        %( page => 'page2001' ),
    );
    is($url, '/entry/page2001/', 'url method test no.2');
    $url = $r.url(
        '/test-this-url',
        %( Nil ),
    );
    is($url, '/test-this-url', 'url method test no.3');
}, 'check url method';

# subtest {
#    plan 1;
#    ok(
#        $r._compile ~~ Regex,
#        '_compile returns Regexp',
#    );
#}, 'check _compile method';

subtest {
    plan 9;
    %h = $r.match('/feedback/Konstantin/kostas/kostas@apopheoz.ru', 'GET');
    ok(
        (
            %h<controller> eq 'Feedback' &&
            %h<action> eq 'getuserdata' &&
            ~(%h<name>) eq 'Konstantin' &&
            ~(%h<nick>) eq 'kostas' &&
            ~(%h<email>) eq 'kostas@apopheoz.ru'
        ),
        'match test no.0',
    );
    %h = $r.match('/dl/foobar.pdf', 'GET');
    ok(
        (
            %h<controller> eq 'Dl' &&
            %h<action> eq 'readfile' &&
            ~(%h<file>) eq 'foobar' &&
            ~(%h<format>) eq 'pdf'
        ),
        'match test no.1',
    );
    %h = $r.match('/dl/foobar.pdf' );
    ok(
        (
            %h<controller> eq 'Dl' &&
            %h<action> eq 'readfile' &&
            ~(%h<file>) eq 'foobar' &&
            ~(%h<format>) eq 'pdf'
        ),
        'match test no.1, with Str http-method',
    );
    %h = $r.match('/dl/foobar.pdf', '' );
    ok(
        (
            %h<controller> eq 'Dl' &&
            %h<action> eq 'readfile' &&
            ~(%h<file>) eq 'foobar' &&
            ~(%h<format>) eq 'pdf'
        ),
        'match test no.1, with \'\' http-method',
    );
    %h = $r.match('/entries/2018/12', 'POST');
    ok(
        (
            %h<controller> eq 'Entries' &&
            %h<action> eq 'show' &&
            ~(%h<year>) eq '2018' &&
            ~(%h<month>) eq '12'
        ),
        'match test no.2',
    );
    %h = $r.match('/faoobar/2018/01', 'POST');
    nok(
        %h,
        'match test no.3',
    );
    is(
        $r.error,
        '404',
        'match test no.3, check error ' ~ $r.error,
    );
    %h = $r.match('/entries/2018/12', 'DELETE');
    nok(
        %h,
        'match test no.4',
    );
    is(
        $r.error,
        '405',
        'match test no.4, check error ' ~ $r.error,
    );
}, 'check match method';

subtest {
    plan 39;
    my %match_obj;
    my %payload = controller => 'Foo';
    ok(
        $r.add(
            :name('home1'),
            :path('/home/sweet/home'),
            :payload( %payload ),
        ),
        'can accept route',
    );
    ok(
        (
            $r.add(
                :name('home2'),
                :path('/'),
                :payload( %payload ),
            ) ~~ Router::Right
        ),
        'returns instance for chaining',
    );
    ok(
        $r.add(
            :name( Str ),
            :path('/'),
            :payload( %payload ),
        ),
        'does not require a route name',
    );
    $raise_except = False;
    try {
        $r.add(
            :name( 'home1' ),
            :path('/'),
            :payload( %payload ),
        );
    } // $raise_except = True;
    ok(
        $raise_except,
        'raises exception if duplicate route name added',
    );
    $raise_except = False;
    try {
        $r.add(
            :name( 'home3' ),
            :path( Str ),
            :payload( %payload ),
        );
    } // $raise_except = True;
    ok(
        $raise_except,
        'requires a route path',
    );
    is $r.route('/unexisted'), Hash.new, 'get unexited route by name';
    is-deeply(
        $r.route('home1')<methods>,
        [],
        'defaults to accepting all methods',
    );
    $r.add(
        :name('home4'),
        :path('/'),
        :payload( %payload ),
        :options('post'),
    );
    is-deeply(
        $r.route('home4')<methods>,
        [ 'POST' ],
        'can be specified by scalar',
    );
    $r.add(
        :name('home5'),
        :path('/'),
        :payload( %payload ),
        :options('POST, DELETE |GET'),
    );
    is-deeply(
        $r.route('home5')<methods>,
        [ 'DELETE', 'GET', 'POST' ],
        'can be specified by scalar with multiple values',
    );
    $r.add(
        :name('home6'),
        :path('PUT /known/path'),
        :payload( %payload ),
        :options('POST, DELETE |GET'),
    );
    is-deeply(
        $r.route('home6')<methods>,
        [ 'DELETE', 'GET', 'POST', 'PUT' ],
        'can be specified as part of route',
    );

    %match_obj = $r.match('/known/path');

    is(
        %match_obj<details><path>,
        '/known/path',
        'a known route path in details'
    );
    is(
        ($r.routes[%match_obj<details><index>][0])<name>,
        'home6',
        'a known route name in details'
    );
    is-deeply(
        payload(:match(%match_obj)),
        %payload,
        'a known route returns payload',
    );

    is-deeply(
        payload(:match($r.match('/known/path'))),
        %payload,
        'a known route returns payload: cached regex',
    );
    my $_r = Router::Right.new;
    is-deeply(
        payload(:match($_r.match('/known/path'))),
        %( Nil ),
        'an unknown route returns undef: no routes defined',
    );
    $_r.add(
        :name('home'),
        :path('PUT /unknown/path'),
        :payload( %payload ),
    );
    is-deeply(
        payload(:match($_r.match('/known/path'))),
        %( Nil ),
        'an unknown route returns undef: one route is defined',
    );
    is-deeply(
        payload(:match($r.match('/home/sweet/home', 'POST'))),
        %payload,
        'a route with no method restriction matches with any method',
    );
    my $_r2 = Router::Right.new;
    $_r2.add(
        :name('put_home'),
        :path('PUT /test/methods/payload'),
        :payload( %( method => 'PUT' )),
    );
    $_r2.add(
        :name('get_home'),
        :path('GET /test/methods/payload'),
        :payload( %( method => 'GET' )),
    );
    is-deeply(
        payload(:match($_r2.match('/test/methods/payload', 'PUT'))),
        %( method => 'PUT' ),
        'a route with matching methods returns payload, test no.1',
    );
    is-deeply(
        payload(:match($_r2.match('/test/methods/payload', 'GET'))),
        %( method => 'GET' ),
        'a route with matching methods returns payload, test no.2',
    );
    is-deeply(
        payload(:match($_r2.match('/test/methods/payload', 'POST'))),
        %( Nil ),
        'a route with differing methods returns Nil',
    );
    $_r2.add(
        :name('download'),
        :path('/dl/{file}{.format}'),
        :payload( %( controller => 'DL' )),
    );
    %h = $_r2.match('/dl/foo');
    ok(
        (
            %h<controller> eq 'DL' &&
            ~(%h<file>) eq 'foo'
        ),
        'matches without extension',
    );
    %h = $_r2.match('/dl/foo.gz');
    ok(
        (
            %h<controller> eq 'DL' &&
            ~(%h<file>) eq 'foo' &&
            ~(%h<format>) eq 'gz'
        ),
        'matches with extension',
    );
    $_r2.add(
        :name('defaultmatch'),
        :path('/foo/{year}/bar'),
        :payload( %payload ),
    );
    ok( $_r2.match('/foo/1916/bar', Str ), 'defaults match <-[/]>+' );
    my $_r3 = Router::Right.new;
    $_r3.add(
        :name('restrictedmatch'),
        :path('/foo/{year:\d+}/bar'),
        :payload( %payload ),
    );
    ok(
        $_r3.match('/foo/1916/bar'),
        'restricted by pattern, test no.1',
    );
    nok(
        $_r3.match('/foo/foorbar/bar'),
        'restricted by pattern, test no.2',
    );
    $_r3.add(
        :name('mergecontent'),
        :path('/entries/{year}/{month}/{day}'),
        :payload( %payload ),
    );
    %h = $_r3.match('/entries/1916/08/11');
    ok(
        (
            %h<controller> eq 'Foo' &&
            ~(%h<year>) eq '1916' &&
            ~(%h<month>) eq '08' &&
            ~(%h<day>) eq '11'
        ),
        'merges placeholder content into payload',
    );
    $raise_except = False;
    try {
        $_r3.add(
            :name( 'duppayload' ),
            :path( '/entry/{year}/{month}/{year}' ),
            :payload( %payload ),
        );
    } // $raise_except = True;
    ok(
        $raise_except,
        'raises exception on duplicate placeholder',
    );
    # url test
    my $url = $_r3.url(
        'mergecontent',
        %( year => '1916', month => '08', day => '14'),
    );
    is($url, '/entries/1916/08/14', 'from route');
    $raise_except = False;
    try {
        $url = $_r3.url(
            'restrictedmatch',
            %( year => 'foo'),
        );
    } // $raise_except = True;
    ok(
        $raise_except,
        'raises exception if invalid placeholder value',
    );
    $url = $_r2.url(
        'download',
        %( file => 'foo', format => 'mp3'),
    );
    is($url, '/dl/foo.mp3', 'with special format placeholder supplied');
    $url = $_r2.url(
        'download',
        %( file => 'foo' ),
    );
    is($url, '/dl/foo', 'with special format placeholder not supplied');
    $_r3.add(
        :name('dl2'),
        :path('/dl/{file}{format}'),
        :payload( %payload ),
    );
    $url = $_r3.url(
        'dl2',
        %( file => 'foo', format => '.mp3' ),
    );
    is($url, '/dl/foo.mp3', 'with non-special format placeholder');
    $url = $r.url('/foo/zort', %(q => 'abc'));
    is($url, '/foo/zort?q=abc', 'from literal url');
    $url = $r.url('/entries/{year}', %(year => '1916', q => 'abc'));
    is($url, '/entries/1916?q=abc', 'from literal url with placeholders');
    $raise_except = False;
    try {
        $url = $_r3.url(
            'unknown',
            %( Nil ),
        );
    } // $raise_except = True;
    ok(
        $raise_except,
        'raises exception if unknown name and not a url',
    );
    $_r3.add(
        :name('allowtest1'),
        :path('GET,PUT,DELETE /allow/methods/test'),
        :payload( %payload ),
    );
    $_r3.match('/allow/methods/test');
    my @a_etalon = ('DELETE', 'GET', 'PUT');
    my @a = $_r3.allowed_methods;
    is-deeply(
        @a,
        @a_etalon,
        'can return list of allowed methods from previous match',
    );
    $_r3.add(
        :name('allowtest2'),
        :path('POST,GET /allow/methods/test/2'),
        :payload( %payload ),
    );
    @a_etalon = ('GET', 'POST');
    @a = $_r3.allowed_methods('allowtest2');
    is-deeply(
        @a,
        @a_etalon,
        'can look up allowed methods by route name',
    );
    $_r3.add(
        :name('allowtest3'),
        :path('POST /allow/methods/test/3'),
        :payload( 'Foo#Bar' ),
    );
    @a_etalon = ('POST');
    @a = $_r3.allowed_methods('/allow/methods/test/3');
    is-deeply(
        @a,
        @a_etalon,
        'can look up allowed methods by route path',
    );
    my Str $s = $_r3.as_string;
    ok($s, 'as_string method test');
    $s.say;
}, 'tests from Perl5 Route::Right';

subtest {
    plan 3;
    my $rmeth = Router::Right.new;
    $rmeth.add(
        :name('put_home'),
        :path('PUT,DELETE /test/methods/payload/p'),
        :payload( %( method => 'PUT' )),
    );
    $rmeth.add(
        :name('get_home'),
        :path('GET /test/methods/payload/p'),
        :payload( %( method => 'GET' )),
    );
    my @a_etalon = ('DELETE', 'GET', 'PUT');
    my @a = $rmeth.allowed_methods('/test/methods/payload/p');
    is-deeply(@a, @a_etalon, 'allowed_methods test no.1');
    @a = $rmeth.allowed_methods('/fake/path');
    nok(@a, 'allowed_methods test no.2');
    @a = $rmeth.allowed_methods;
    nok(@a, 'allowed_methods test no.3');
}, 'check allowed_methods method';

CATCH {
    default {
        say .message;
        say .backtrace.full;
    }
}

done-testing;

sub payload(Hash :$match) returns Hash {
    my Hash $r = $match;

    $r<details>:delete;

    $r;
}
