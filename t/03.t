use v6;
use Test;

use Router::Right;

plan 4;

my %h;
my @a;
sub trimmed( Str $str ) returns Str {
    my $s = $str;
    $s ~~ s:Perl5:g/^\s+|\s+$//;
    $s ~~ s:Perl5:g/\s+/ /;
    $s;
}

subtest {
    plan 2;
    my $r = Router::Right.new;
    is( $r.as_string, Str, 'no routes to start for subtest no.1' );
    $r.resource( :member('message') );
    my $expected = qq{
                 message_set GET    /message{.format}           {action => index}
                             POST   /message{.format}           {action => create}
       formatted_message_set GET    /message.{format}           {action => index}
                 new_message GET    /message/new{.format}       {action => new}
       formatted_new_message GET    /message/new.{format}       {action => new}
                     message GET    /message/{id}{.format}      {action => show}
                             PUT    /message/{id}{.format}      {action => update}
                             DELETE /message/{id}{.format}      {action => delete}
           formatted_message GET    /message/{id}.{format}      {action => show}
                edit_message GET    /message/{id}{.format}/edit {action => edit}
      formatted_edit_message GET    /message/{id}.{format}/edit {action => edit}
    };
    is(
        trimmed($r.as_string),
        trimmed($expected),
        'can be added',
    );
}, 'P5 Resources subtest no.1';

subtest {
    plan 2;
    my $r = Router::Right.new;
    is( $r.as_string, Str, 'no routes to start for subtest no.2' );
    $r.resource( :member('message'), :options( %( collection => 'mailboxes' ) ) );
    my $expected = q{
                   mailboxes GET    /mailboxes{.format}           {action => index}
                             POST   /mailboxes{.format}           {action => create}
         formatted_mailboxes GET    /mailboxes.{format}           {action => index}
                 new_message GET    /mailboxes/new{.format}       {action => new}
       formatted_new_message GET    /mailboxes/new.{format}       {action => new}
                     message GET    /mailboxes/{id}{.format}      {action => show}
                             PUT    /mailboxes/{id}{.format}      {action => update}
                             DELETE /mailboxes/{id}{.format}      {action => delete}
           formatted_message GET    /mailboxes/{id}.{format}      {action => show}
                edit_message GET    /mailboxes/{id}{.format}/edit {action => edit}
      formatted_edit_message GET    /mailboxes/{id}.{format}/edit {action => edit}
    };
    is(
        trimmed($r.as_string),
        trimmed($expected),
        'can override plural collection name',
    );
}, 'P5 Resources subtest no.2';

# skip-rest 'Reason: subtest no.3 is throwing exception';
# exit;

subtest {
    plan 2;
    my $r = Router::Right.new;
    is( $r.as_string, Str, 'no routes to start for subtest no.3' );
    $r.with(
        :name('a'),
        :path('/a'),
        :options( %( payload => %( controller => 'A' ) ) ),
    ).with(
        :name('b'),
        :path('/b'),
        :options( %( payload => %( controller => '::B' ) ) ),
    ).with(
        :name('c'),
        :path('/c'),
        :options( %( payload => %( controller => '::C' ) ) ),
    ).resource(
        :member('user'),
        :options( %( payload => %( controller => '::User' ) ) ),
    );
    my $expected = qq{
           a_b_c_user_set GET    /a/b/c/user{.format}           {action => index, controller => A::B::C::User}
                          POST   /a/b/c/user{.format}           {action => create, controller => A::B::C::User}
 a_b_c_formatted_user_set GET    /a/b/c/user.{format}           {action => index, controller => A::B::C::User}
           a_b_c_new_user GET    /a/b/c/user/new{.format}       {action => new, controller => A::B::C::User}
 a_b_c_formatted_new_user GET    /a/b/c/user/new.{format}       {action => new, controller => A::B::C::User}
               a_b_c_user GET    /a/b/c/user/{id}{.format}      {action => show, controller => A::B::C::User}
                          PUT    /a/b/c/user/{id}{.format}      {action => update, controller => A::B::C::User}
                          DELETE /a/b/c/user/{id}{.format}      {action => delete, controller => A::B::C::User}
     a_b_c_formatted_user GET    /a/b/c/user/{id}.{format}      {action => show, controller => A::B::C::User}
          a_b_c_edit_user GET    /a/b/c/user/{id}{.format}/edit {action => edit, controller => A::B::C::User}
a_b_c_formatted_edit_user GET    /a/b/c/user/{id}.{format}/edit {action => edit, controller => A::B::C::User}
    };
    is(
        trimmed($r.as_string),
        trimmed($expected),
        'can be nested',
    );
}, 'P5 Resources subtest no.3';

subtest {
    plan 2;
    my $r = Router::Right.new;
    is(
        $r.as_string,
        Str,
        'no routes to start for nested methods test',
    );
    $r.with(
        :name('a'),
        :path('/a'),
        :options( %( methods => 'A', payload => %( controller => 'A' ) ) ),
    ).with(
        :name('b'),
        :path('/b'),
        :options( %( methods => 'B', payload => %( controller => '::B' ) ) ),
    ).with(
        :name('c'),
        :path('/c'),
        :options( %( methods => 'C', payload => %( controller => '::C' ) ) ),
    ).resource(
        :member('user'),
        :options( %( payload => %( controller => '::User' ) ) ),
    );
    my $expected = qq{
           a_b_c_user_set A,B,C,GET    /a/b/c/user{.format}           {action => index, controller => A::B::C::User}
                          A,B,C,POST   /a/b/c/user{.format}           {action => create, controller => A::B::C::User}
 a_b_c_formatted_user_set A,B,C,GET    /a/b/c/user.{format}           {action => index, controller => A::B::C::User}
           a_b_c_new_user A,B,C,GET    /a/b/c/user/new{.format}       {action => new, controller => A::B::C::User}
 a_b_c_formatted_new_user A,B,C,GET    /a/b/c/user/new.{format}       {action => new, controller => A::B::C::User}
               a_b_c_user A,B,C,GET    /a/b/c/user/{id}{.format}      {action => show, controller => A::B::C::User}
                          A,B,C,PUT    /a/b/c/user/{id}{.format}      {action => update, controller => A::B::C::User}
                          A,B,C,DELETE /a/b/c/user/{id}{.format}      {action => delete, controller => A::B::C::User}
     a_b_c_formatted_user A,B,C,GET    /a/b/c/user/{id}.{format}      {action => show, controller => A::B::C::User}
          a_b_c_edit_user A,B,C,GET    /a/b/c/user/{id}{.format}/edit {action => edit, controller => A::B::C::User}
a_b_c_formatted_edit_user A,B,C,GET    /a/b/c/user/{id}.{format}/edit {action => edit, controller => A::B::C::User}
    };
    is(
        trimmed($r.as_string),
        trimmed($expected),
        'methods can be nested too',
    );
}, 'nested methods';
